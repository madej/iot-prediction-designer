The **Internet Of Things** project which merge IoT standards with prediction engine.

- The project is based in red-node project
- The prediction engine: C# WebAPI available from RESTful.
- The prediction allows forecast machine's status, malfunctions and maintenance. The Internet Of Things prediction project accepts data from any source like sensors, database, etc.