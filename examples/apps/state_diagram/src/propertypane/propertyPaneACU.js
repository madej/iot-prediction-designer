example.propertypane.PropertyPaneAcuObject = Class.extend({
	
	init:function(stateFigure){
	    this.figure = stateFigure;
	},
	
	injectPropertyView: function( domId)
	{
	    var view = $(
	                 "<div class='control-group'>"+
	                 "   <label class='control-label'>ACU. additional properties below... </label>"+
					 "<div class='controls'>"+
	                 "      <label>Name: </label><input id='stateNameProperty' class='input-xlarge' type='text' value='"+this.figure.getLabel()+"'/>"+
	                 "   </div>"+
	                 //"</div>"+
					 "<div class='controls'>"+
	                 "      <label>GUID: </label><input id='stateGUIDProperty' class='input-xlarge' type='text' value='"+this.figure.getGUID()+"'/>"+
	                 "   </div>"+
	                 "</div>"+
					 "</div>"
					 /*
					 "<div class='btn-group' dropdown is-open='status.isopen'>"+
					"<button type='button' class='btn btn-primary dropdown-toggle' dropdown-toggle ng-disabled='disabled'>"+
					"Button dropdown <span class='caret'></span></button>"+
					"<ul class='dropdown-menu' role='menu'>"+
					"<li><a href='#'>Action</a></li>"+
					"<li><a href='#'>Another action</a></li>"+
					"<li><a href='#'>Something else here</a></li>"+
					"<li class='divider'></li>"+
					"<li><a href='#'>Separated link</a></li></ul></div>"
					*/
	               );
				   
		var input = view.find("#stateNameProperty");
	    var handler =$.proxy(function(e){
	        e.preventDefault();
            // provide undo/redo for the label field
            app.executeCommand(new example.command.CommandSetLabel(this.figure, input.val()));
	    },this);
	    input.change(handler);
	    view.submit(function(e){
	        return false;
	    });
	    
	    domId.append(view);
	},

    /**
     * @method
     * called by the framework if the pane has been resized. This is a good moment to adjust the layout if
     * required.
     * 
     */
    onResize: function()
    {
    },
    

    /**
     * @method
     * called by the framework before the pane will be removed from the DOM tree
     * 
     */
    onHide: function()
    {
    }
    

    



});

