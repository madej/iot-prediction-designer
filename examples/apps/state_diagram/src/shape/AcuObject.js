var AcuObject = draw2d.shape.basic.Image.extend({

                NAME: "AcuObject",

                path: "./img/Acu32_refresh50.bmp",

                init: function(attr, setter, getter) {

					//this._super(attr);
					
                    this._super(
					$.extend({
						width:32,
						height:32,
						x:100,
						y:100},attr),
					setter,
					getter);
					
					//this.setBoundingBox(new draw2d.geo.Rectangle(1, 200, 70, 70));

                    // Ports.
                    this.createPort("output");
                    this.createPort("output");

                    this.setUserData({
                        dany: 1
                    });
					
					// Label
					this.label = new draw2d.shape.basic.Label({text:"ACU", color:"#0d0d0d", fontColor:"#0d0d0d"});
					this.add(this.label, new draw2d.layout.locator.BottomLocator(this));
					this.label.installEditor(new draw2d.ui.LabelInplaceEditor());
                },
				onContextMenu:function(x,y){
					$.contextMenu({
						selector: 'body', 
					events:
					{  
						hide:function(){ $.contextMenu( 'destroy' ); }
					},
					callback: $.proxy(function(key, options) 
					{
					switch(key){
					case "addDoor":
						this.AddDoor();
						break;
					case "addRelay":
						this.AddRelay();
						break;
					case "open":
						this.setPath('./img/Door32_open.bmp');
						break;
					case "connections":
						this.ShowConnections();
						break;
					case "alarm":
						this.setColor('#00A8F0');
						break;
					case "delete":
						// without undo/redo support
						//     this.getCanvas().remove(this);
                   
						// with undo/redo support
						var cmd = new draw2d.command.CommandDelete(this);
						this.getCanvas().getCommandStack().execute(cmd);
					default:
						break;
					}
            
				},this),
            x:x,
            y:y,
            items: 
            {
                "open":    {name: "Open"},
				"addDoor": {name: "Add door"},
				"addRelay": {name: "Add relay"},
                "connections":  {name: "Show/Hide connections"},
                "alarm":   {name: "Alarm"},
                "delete": {name: "Delete"}
            }
        });
   }, 
   ShowConnections: function(){
	   this.getPorts().each(function(i,port){
				port.getConnections().each(function(i, connection){
				connection.setVisible(!connection.isVisible());
		})
    });
   },
   AddRelay: function(){
	   var canv = this.getCanvas();
	   
	   var relayDevice = new RelayDevice({x:this.x+30,y:this.y+30});
	   canv.add(relayDevice);
	   
	   //var readerDevA = new ReaderDevice({x:this.x+30,y:this.y+30});
	   //canv.add(readerDevA);
   },
   AddDoor: function(){
	   var canv = this.getCanvas();
	   
	   var doorCtrl = new DoorController({x:this.x+30,y:this.y+30});
	   canv.add(doorCtrl);
   },
   /**
      * @method
      * Set the text to show if the state shape
      * 
      * @param {String} text
      */
     setLabel: function (text)
     {
         this.label.setText(text);
         
         return this;
     },
   /**
      * @method
      * Set the text to show if the state shape
      * 
      *     {
      *       activity: "activityId"
      *       mapping: [{ parameterName: "nameOfTheParameter",
      *                   value: "theVariableOfTheContext"
      *                 },
      *                 ...
      *                ]
      *     }

      * @param {Object} activityDef the setting for the activity
      */
     setActivity: function (activityDef)
     {
    	 this.activityLabel.setText(activityDef.activity);
         this.setUserData(activityDef);
         
         return this;
     },
   /**
      * @method
      * Return the label of the shape
      * 
      */
     getLabel: function ()
     {
         return this.label.getText();
     },
	 getGUID: function(){
		 return "GUID";
	 },
	 setGUID: function(data){
		 this.setUserData({
                        GUID: data
                    });
	 }
});